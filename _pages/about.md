---
layout: page
title: About us
description: DC20e6 is a community for people interested in studying modern technologies and information security.
permalink: /about/
---

[DC20e6](https://dc20e6.ru/) is a community for people interested in studying modern technologies and applying them in an unusual way which often implies information security and everything the positive meaning of the word "hacking" includes.


