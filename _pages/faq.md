---
layout: page
title: FAQ
permalink: /faq/
---

## Does it affect me? Why do I need it?

- If you don't use any USB device at all, then, of course, you don't need a USBCondom. But how can you read this page? :)
- But if you have USB cables for charging phones, flash drives and everything that works with USB, we would recommend such protection. If you charge your phone in cafes, airports or offices using public USB ports, all your data may fall into the wrong hands. Or it can fry your device.

## Could you tell more about these attacks?

- **Juice jacking** is a type of cyber attack involving a charging port that doubles as a data connection. This often involves either installing malware or surreptitiously copying sensitive data from a smart phone, tablet, or other computer device. [link]
- **USB Killer** is a device appearing as a USB thumb drive that sends high-voltage power surges into the device it is connected to, threatening to damage hardware components. The device has been designed to test components for protection from power surges and electrostatic discharge.
- **BadUSB**. [https://www.youtube.com/watch?v=nuruzFqMgIw](https://www.youtube.com/watch?v=nuruzFqMgIw) [https://en.wikipedia.org/?title=BadUSB](https://en.wikipedia.org/?title=BadUSB) [https://www.wired.com/2014/07/usb-security/](https://www.wired.com/2014/07/usb-security/) 

## Some news and articles about attacks made via USB

- Former Student Used “USB Killer Device” to Destroy Computer Equipment and Caused More than $58,000 in Damage: [https://www.justice.gov/usao-ndny/pr/former-student-pleads-guilty-destroying-computers-college-st-rose](https://www.justice.gov/usao-ndny/pr/former-student-pleads-guilty-destroying-computers-college-st-rose)
- Malicious Software Campaign Targets Apple Users in China: [https://bits.blogs.nytimes.com/2014/11/05/malicious-software-campaign-targets-apple-users-in-china/](https://bits.blogs.nytimes.com/2014/11/05/malicious-software-campaign-targets-apple-users-in-china/)
- Another Cyberattack Spotted Targeting Mideast Critical Infrastructure Organizations: [https://www.darkreading.com/attacks-breaches/another-cyberattack-spotted-targeting-mideast-critical-infrastructure-organizations/d/d-id/1330679](https://www.darkreading.com/attacks-breaches/another-cyberattack-spotted-targeting-mideast-critical-infrastructure-organizations/d/d-id/1330679)
- Вредоносные USB-устройства как вектор атаки: [https://www.kaspersky.ru/blog/weaponized-usb-devices/22648/](https://www.kaspersky.ru/blog/weaponized-usb-devices/22648/)

[![USB Killer… WHY???](http://img.youtube.com/vi/y_bbX_Ch1Z8/0.jpg)](https://www.youtube.com/watch?v=y_bbX_Ch1Z8)

## USB Condom is Open-source hardware

All Condoms are open sourced, and our repositories are open to your Pull Requests to make the project better :)

Scheme and BOM (Bill of materials) are available: usb-c0nd0m (main) and a fork at GitHub.

## Who is DC20e6?

DC20e6 is a community for people interested in studying modern technologies and applying them in an unusual way which often implies information security and everything the positive meaning of the word "hacking" includes.

---

> Q: Why should I care? These attacks are complicated and require some knowledge.
>
> A: NO! There are easy and cheap techniques to kill your device or steal your sensitive information.

## Beware, all of these tools are widely available for purchase if one wants to buy them.

- **~$3** USB Killer [https://hackernoon.com/this-3-diy-usb-device-will-kill-your-computer-33c4bdb1da40](https://hackernoon.com/this-3-diy-usb-device-will-kill-your-computer-33c4bdb1da40)
- [USB KILLER](https://usbkill.com/) - **~$35** USD on Aliexpress or Ebay
- [WiFi HID Injector - An USB Rubberducky / BadUSB On Steroids.](https://github.com/whid-injector/WHID) **~$14** USD on Aliexpress
- [USBNinja](https://sneaktechnology.com/product/usbninja-basic/) **~$99** USD


## You can make BadUSB device on your own for FREE (if you have Rapsberry Pi Zero W)

- [PoisonTap](https://samy.pl/poisontap/) - siphons cookies, exposes internal router & installs web backdoor on locked computers
- [P4wnP1 A.L.O.A. by MaMe82](https://github.com/RoganDawes/P4wnP1_aloa) is a framework which turns a Rapsberry Pi Zero W into a flexible, low-cost platform for pentesting, red teaming and physical engagements ... or into "A Little Offensive Appliance".
- [DeviceKey from DC20e6](https://gitlab.com/dc20e6/devicekey) is a framework for testing any USB devices.
