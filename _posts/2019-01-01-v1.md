---
layout: post
title:  USBCondom v1
description: The first version, which is very easy to assemble ;)
date:   2019-01-01 00:00:00 +0400
image:  '/images/v1/main.jpg'
tags:   [usb, usb-condom, 5min]
---

> The simplest version of USB Condom that is assembled in a couple of minutes.

To make such a device, you need:
- 2 USB-A connectors (male and female)
- Soldering iron

## How-to
1. Remove two central pins from USB connectors (with fingers, tweezers, pliers or something else).
2. Solder GND to GND аnd VCC to VCC  (with the correct pinout [img](https://pinouts.ru/visual/USB.jpg)).
3. Put on the heat shrink to make it look prettier :)

<div class="gallery-box">
  <div class="gallery">
    <img src="/images/v1/1.jpg" alt="Step 1">
    <img src="/images/v1/2.jpg" alt="Step 2">
  </div>
</div>

<div class="gallery-box">
  <div class="gallery">
    <img src="/images/v1/3.jpg" alt="Step 3">
    <img src="/images/v1/4.jpg" alt="Step 4">
  </div>
</div>

## Analogs
- Qi charger
